#include "BTree.h"

int main() {
	

	try
	{
		BTree<int> tree;
		tree.insert(10);
		tree.insert(5);
		tree.insert(19);
		tree.insert(6);

		int order_code;
		cout << "Choose how you want to make tree traversal? 1 - Preorder, 2 - Inorder, 3 - Postorder" << endl;
		cin >> order_code;
		cout << "Yours tree nodes is:" << endl;

		tree.treeTraversal(order_code);

		cout << tree.has(87); // 1 - true, 0 - false ?
	}
	catch (const std::runtime_error& er)
	{
		cout << er.what();
	}
	return 0;
}