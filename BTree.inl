
template<typename T>
void BTree<T>::each(function<void(Node<T>& node)> callback, Order order_code) {
	
	switch (order_code)
	{
	case Order::PreOrder:
		eachPreorder(callback, root_);
		break;

	case Order::InOrder:
		eachInorder(callback, root_);
		break;

	case Order::PostOrder:
		eachPostorder(callback, root_);
		break;
	default:
		throw runtime_error("Invalid input");
		break;
	}
}

template<typename T>
void BTree<T>::each(function<void(const Node<T>& node)> callback, Order order_code) const{

	switch (order_code)
	{
	case Order::PreOrder:
		eachPreorder(callback, root_);
		break;

	case Order::InOrder:
		eachInorder(callback, root_);
		break;

	case Order::PostOrder:
		eachPostorder(callback, root_);
		break;
	default:
		throw runtime_error("Invalid input");
		break;
	}
}

template<typename T>
void BTree<T>::each(function<void(T& data)> callback) {
	each_(callback, root_);
}

template<typename T>
void BTree<T>::each(function<void(const T& data)> callback) const{
	each_(callback, root_);
}

template<typename T>
void BTree<T>::treeTraversal(Order order_code) {
	each([](const Node<int>& node) {
		cout << node.data << endl;
	}, order_code);
}

template<typename T>
void BTree<T>::print() {
	print_(root_);
}

template<typename T>
void BTree<T>::insert(const T & data) {
	auto* newNode = new Node<T>;
	newNode->data = data;


	if (empty()) {
		root_ = newNode;
	}
	else {
		auto iter = root_;

		while (iter) {
			if (data >= iter->data) {
				if (iter->right) {
					iter = iter->right;
				}
				else {
					iter->right = newNode;
					newNode->parent = iter;
					break;
				}
			}
			else {
				if (iter->left) {
					iter = iter->left;
				}
				else {
					iter->left = newNode;
					newNode->parent = iter;
					break;
				}
			}
		}
	}

	size_++;
}

template<typename T>
bool BTree<T>::empty() const {
	return !root_;
}

template<typename T>
void BTree<T>::erase(const T& data) {
	if (empty()) {
		throw runtime_error("Empty tree");
	}
	else {
		auto iter = root_;

		while (iter) {
			if (iter->data == data) {
				delete iter;
				iter = nullptr;
			}
			else {
				if (data > iter->data) {
					iter = iter->right;
				}
				else {
					iter = iter->left;
				}
			}
		}
		size_--;
		
	}
}

template<typename T>
bool BTree<T>::has(const T& data) {
	bool value = false;

	each([&data, &value](const T& data_) {
		if (data_ == data) {
			value = true;
		}
	});
	
	return value;
}

template<typename T>
void BTree<T>::print_(Node<T>* node) {

	if (!node) {
		return;
	}

	cout << node->data << endl;
	print_(node->left);
	print_(node->right);
}

template<typename T>
void BTree<T>::eachPreorder(function<void(Node<T>&)> callback, Node<T>* node){
	if (!node) {
		return;
	}

	callback(*node);
	eachPreorder(callback, node->left);
	eachPreorder(callback, node->right);
}

template<typename T>
void BTree<T>::eachInorder(function<void(Node<T>&)> callback, Node<T>* node){
	if (!node) {
		return;
	}

	eachInorder(callback, node->left);
	callback(*node);
	eachInorder(callback, node->right);
}

template<typename T>
void BTree<T>::eachPostorder(function<void(Node<T>&)> callback, Node<T>* node){
	if (!node) {
		return;
	}

	eachPostorder(callback, node->left);
	eachPostorder(callback, node->right);
	callback(*node);
}

template<typename T>
void BTree<T>::eachPreorder(function<void(const Node<T>&)> callback, Node<T>* node) const{
	if (!node) {
		return;
	}

	callback(*node);
	eachPreorder(callback, node->left);
	eachPreorder(callback, node->right);
}

template<typename T>
void BTree<T>::eachInorder(function<void(const Node<T>&)> callback, Node<T>* node) const{
	if (!node) {
		return;
	}

	eachInorder(callback, node->left);
	callback(*node);
	eachInorder(callback, node->right);
}

template<typename T>
void BTree<T>::eachPostorder(function<void(const Node<T>&)> callback, Node<T>* node) const{
	if (!node) {
		return;
	}

	eachPostorder(callback, node->left);
	eachPostorder(callback, node->right);
	callback(*node);
}

template<typename T>
void BTree<T>::each_(function<void(T&)> callback, Node<T>* node) {
	if (!node) {
		return;
	}

	callback(node->data);
	each_(callback, node->left);
	each_(callback, node->right);
}

template<typename T>
void BTree<T>::each_(function<void(const T&)> callback, Node<T>* node) const {
	if (!node) {
		return;
	}

	callback(node->data);
	each_(callback, node->left);
	each_(callback, node->right);
}