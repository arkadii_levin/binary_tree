#pragma once
#include <iostream>
#include <functional>

using namespace std;

//
//������������ ������ :
//-erase � ��������� ������ ����� �������� ��������, ������� ����� ����� � �������
//- has � ��������� ������ ����� �������� ��������, ������� ����� ���������, ������� �� ����� � ����� �������� ������
//- each
//��������� ���.inl � �++
//���������� ���������� �������� � ���� ����
//���������� ���������� ��������� ������ �.h �����

template<typename T>
class Node
{
public:
	T data;
	Node<T>* left = nullptr;
	Node<T>* right = nullptr;
	Node<T>* parent = nullptr;
};
 
template<typename T>
class BTree
{
public:
	enum class Order
	{
		PreOrder,
		InOrder,
		PostOrder
	};

	void each(function<void(Node<T>& node)> callback, Order order_code);
	void each(function<void(const Node<T>& node)> callback, Order order_code) const;
	void each(function<void(T& data)> callback);
	void each(function<void(const T& data)> callback) const;

	void treeTraversal(Order order_code);

	void print();

	void insert(const T& data);

	bool empty() const;

	void erase(const T& data);

	bool has(const T& data);

private:
	Node<T>* root_ = nullptr;
	size_t size_ = 0u;

	void print_(Node<T>* node);
	void eachPreorder(function<void(Node<T>& node)> callback, Node<T>* node);
	void eachInorder(function<void(Node<T>& node)> callback, Node<T>* node);
	void eachPostorder(function<void(Node<T>& node)> callback, Node<T>* node);
	void eachPreorder(function<void(const Node<T>& node)> callback, Node<T>* node) const;
	void eachInorder(function<void(const Node<T>& node)> callback, Node<T>* node) const;
	void eachPostorder(function<void(const Node<T>& node)> callback, Node<T>* node) const;

	void each_(function<void(T&)> callback, Node<T>* node);
	void each_(function<void(const T&)> callback, Node<T>* node) const;
};

#include "BTree.inl"